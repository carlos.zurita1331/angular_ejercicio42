import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/servicios/validadores.service';


@Component({
  selector: 'app-formulario42',
  templateUrl: './formulario42.component.html',
  styleUrls: ['./formulario42.component.css']
})
export class Formulario42Component implements OnInit {

    forma!: FormGroup;


  constructor(private fb: FormBuilder,
              private validadores: ValidadoresService) {
    this.crearformulario42();
   }

  ngOnInit(): void {
  }

  get nombreNoValido(){
    return this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.touched;
  }

  get apellidoNoValido(){
    return this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.touched;
  }

  get NombreNoZaida(){
    return (this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.errors?.['noZaida']) && this.forma.get('nombre')?.touched;
  }

  get NombreNoEveneser(){
    return (this.forma.get('nombre')?.invalid && this.forma.get('nombre')?.errors?.['noEveneser']) && this.forma.get('nombre')?.touched;
  }

  get ApellidoNoFarella(){
    return (this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.errors?.['noFarella'])&& this.forma.get('apellido')?.touched;
  }

  get ApellidoNoMeleneses(){
    return (this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.errors?.['noMeleneses'])&& this.forma.get('apellido')?.touched;
  }

  get ApellidoNoPrieto(){
    return (this.forma.get('apellido')?.invalid && this.forma.get('apellido')?.errors?.['noPrieto'])&& this.forma.get('apellido')?.touched;
  }


  crearformulario42():void{
    this.forma = this.fb.group({
      nombre:['',[Validators.required ,Validators.maxLength(10),this.validadores.noZaida,this.validadores.noEveneser]],
      apellido:['',[Validators.required, Validators.maxLength(10),this.validadores.noFarella,this.validadores.noMeleneses,this.validadores.noPrieto]],
      edad:['']
    });
   }

  guardar():void{
    console.log(this.forma.value);
    
  }

  limpiar():void{
    this.forma.reset()
  }

  get CalculateAge(): number {
    const today: Date = new Date();
    const birthDate: Date = new Date(this.forma.get('edad')?.value);
    let age: number = today.getFullYear() - birthDate.getFullYear();
    const month: number = today.getMonth() - birthDate.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
        age--;
        
    }
    console.log(age);
    
    return age;
  }

} 
