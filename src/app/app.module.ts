import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule, Validator} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Formulario42Component } from './components/formulario42/formulario42.component';

@NgModule({
  declarations: [
    AppComponent,
    Formulario42Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
